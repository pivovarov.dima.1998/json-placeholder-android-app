package com.filtcher.jsonplaceholderapp.data

sealed class RepositoryResultState<out R>{
    object Inited : RepositoryResultState<Nothing>()
    object Loading : RepositoryResultState<Nothing>()
    data class Success<out T> (val data : T) : RepositoryResultState<T>()
    data class Error (val error: Throwable) : RepositoryResultState<Nothing>()
}