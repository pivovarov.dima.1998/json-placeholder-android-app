package com.filtcher.jsonplaceholderapp.data.models

data class Album(
    val id: Int,
    val title: String,
    val userId: Int
)