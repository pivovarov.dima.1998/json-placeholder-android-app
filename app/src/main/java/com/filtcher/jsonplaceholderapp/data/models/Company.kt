package com.filtcher.jsonplaceholderapp.data.models

data class Company(
    val bs: String,
    val catchPhrase: String,
    val name: String
)