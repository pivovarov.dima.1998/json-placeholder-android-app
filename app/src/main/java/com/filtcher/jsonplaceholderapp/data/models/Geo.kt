package com.filtcher.jsonplaceholderapp.data.models

data class Geo(
    val lat: String,
    val lng: String
)