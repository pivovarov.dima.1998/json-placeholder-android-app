package com.filtcher.jsonplaceholderapp.data.network

import com.filtcher.jsonplaceholderapp.data.models.Album
import com.filtcher.jsonplaceholderapp.data.models.Photo
import com.filtcher.jsonplaceholderapp.data.models.User
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface NetworkService {

    @GET("users")
    suspend fun getUsers(): Response<List<User>>

    @GET("users/{id}/albums")
    suspend fun getAlbumsByUserId(@Path("id") id: Int): Response<List<Album>>

    @GET("albums")
    suspend fun getAlbums(): Response<List<Album>>

    @GET("albums/{id}/photos")
    suspend fun getPhotosByAlbumId(@Path("id") id: Int): Response<List<Photo>>

    @GET("photos")
    suspend fun getPhotos(): Response<List<Photo>>


}