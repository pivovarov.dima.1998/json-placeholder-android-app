package com.filtcher.jsonplaceholderapp.data.repositories

import com.filtcher.jsonplaceholderapp.data.RepositoryResultState
import com.filtcher.jsonplaceholderapp.data.network.RetrofitInstance
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import retrofit2.Response

class ApiRepository {

    private fun <T> parseResult(response: Response<T>) : RepositoryResultState<T>{
        return try {
            if (response.isSuccessful) {
                RepositoryResultState.Success(response.body()!!)
            } else {
                throw HttpException(response)
            }
        } catch (error: Throwable) {
            RepositoryResultState.Error(error)
        }
    }

    fun getUsers() = flow{
        emit(RepositoryResultState.Loading)
        val response = RetrofitInstance.api.getUsers()
        emit(parseResult(response))
    }

    fun getAlbumsByUserId(userId: Int) = flow{
        emit(RepositoryResultState.Loading)
        val response = RetrofitInstance.api.getAlbumsByUserId(userId)
        emit(parseResult(response))
    }

    fun getPhotosByAlbumId(albumId: Int) = flow {
        emit(RepositoryResultState.Loading)
        val response = RetrofitInstance.api.getPhotosByAlbumId(albumId)
        emit(parseResult(response))
    }

    fun getPhotos() = flow {
        emit(RepositoryResultState.Loading)
        val response = RetrofitInstance.api.getPhotos()
        emit(parseResult(response))
    }

    fun getAlbums() = flow {
        emit(RepositoryResultState.Loading)
        val response = RetrofitInstance.api.getAlbums()
        emit(parseResult(response))
    }
}