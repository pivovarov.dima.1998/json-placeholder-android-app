package com.filtcher.jsonplaceholderapp.ui.screens.users

import android.util.Log
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Card
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.filtcher.jsonplaceholderapp.data.RepositoryResultState
import com.filtcher.jsonplaceholderapp.data.models.User

@Composable
fun UsersScreen(viewModel: UsersScreenViewModel = viewModel()) {
    val state = viewModel.usersStateFlow.collectAsState()
    Log.i("TEST", "${state.value}")
    if (state.value is RepositoryResultState.Loading) Box(modifier = Modifier.fillMaxSize(), Alignment.Center){
        CircularProgressIndicator()
    }
    if (state.value is RepositoryResultState.Success) LazyColumn(){
        (state.value as RepositoryResultState.Success<List<User>>).data.map { user ->
            item {
                Card(
                    Modifier
                        .fillMaxWidth()
                        .padding(16.dp)) {
                    Column() {
                    Text(text = user.name, modifier = Modifier.padding(16.dp))
                        Text(text = user.email, modifier = Modifier.padding(16.dp))
                        Text(text = user.phone, modifier = Modifier.padding(16.dp))
                        Text(text = user.website, modifier = Modifier.padding(16.dp))
                        Text(text = user.address.city, modifier = Modifier.padding(16.dp))

                    }
                }
            }
        }
    }




}
