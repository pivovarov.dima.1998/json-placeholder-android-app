package com.filtcher.jsonplaceholderapp.ui.screens.users

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.filtcher.jsonplaceholderapp.data.RepositoryResultState
import com.filtcher.jsonplaceholderapp.data.models.User
import com.filtcher.jsonplaceholderapp.data.repositories.ApiRepository
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*

class UsersScreenViewModel() : ViewModel() {
    private val _usersStateFlow = MutableStateFlow<RepositoryResultState<List<User>>>(RepositoryResultState.Inited)
    val usersStateFlow = _usersStateFlow.asStateFlow()

    init {
        refreshUsers()
    }

    fun refreshUsers()  {
        val repository = ApiRepository()
        repository.getUsers().onEach {
            delay(5000)
            _usersStateFlow.tryEmit(it)
        }.launchIn(viewModelScope)
    }
}