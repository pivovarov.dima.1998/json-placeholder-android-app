package com.filtcher.jsonplaceholderapp

import com.filtcher.jsonplaceholderapp.data.RepositoryResultState
import com.filtcher.jsonplaceholderapp.data.repositories.ApiRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class ApiUnitTest {

    private val repo = ApiRepository()

    private fun <T> parseResultState(state: RepositoryResultState<T>): Boolean {
        when (state) {
            is RepositoryResultState.Loading -> {
                println("Loading")
                return true
            }
            is RepositoryResultState.Error -> {
                println(state.error.message)
                return false
            }
            is RepositoryResultState.Success -> {
                println("Success")
                return true
            }
            else -> {
                return false
            }
        }
    }

    @Test
    fun getUsers() = runTest {
        repo.getUsers().onEach { state ->
            if (!parseResultState(state)) assert(false)
        }.launchIn(this)
        advanceUntilIdle()
        assert(true)
    }

    @Test
    fun getAlbumsByUserId() = runTest {
        repo.getAlbumsByUserId(1).onEach { state ->
            if (!parseResultState(state)) assert(false)
        }.launchIn(this)
        advanceUntilIdle()
        assert(true)
    }

    @Test
    fun getPhotosByAlbumId() = runTest {
        repo.getPhotosByAlbumId(1).onEach { state ->
            if (!parseResultState(state)) assert(false)
        }.launchIn(this)
        advanceUntilIdle()
        assert(true)
    }

    @Test
    fun getPhotos() = runTest {
        repo.getPhotos().onEach { state ->
            if (!parseResultState(state)) assert(false)
        }.launchIn(this)
        advanceUntilIdle()
        assert(true)
    }

    @Test
    fun getAlbums() = runTest {
        repo.getAlbums().onEach { state ->
            if (!parseResultState(state)) assert(false)
        }.launchIn(this)
        advanceUntilIdle()
        assert(true)
    }
}