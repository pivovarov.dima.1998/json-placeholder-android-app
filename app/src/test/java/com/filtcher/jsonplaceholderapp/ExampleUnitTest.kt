package com.filtcher.jsonplaceholderapp

import com.filtcher.jsonplaceholderapp.data.RepositoryResultState
import org.junit.Assert.assertEquals
import org.junit.Test


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun test() {
        var testResult: RepositoryResultState<String> = RepositoryResultState.Loading

        print("Loading = ")
        when (testResult) {
            is RepositoryResultState.Loading -> println("Loading")
            is RepositoryResultState.Success -> println("Success")
            is RepositoryResultState.Error -> println("Error")
            is RepositoryResultState.Inited -> println("Inited")
            else -> println("Unknown")
        }
        testResult = RepositoryResultState.Success("123")
        print("Success = ")
        when (testResult) {
            is RepositoryResultState.Loading -> println("Loading")
            is RepositoryResultState.Success -> println("Success")
            is RepositoryResultState.Error -> println("Error")
            is RepositoryResultState.Inited -> println("Inited")
            else -> println("Unknown")
        }
        testResult = RepositoryResultState.Error(Throwable("123"))
        print("Error = ")
        when (testResult) {
            is RepositoryResultState.Loading -> println("Loading")
            is RepositoryResultState.Success -> println("Success")
            is RepositoryResultState.Error -> println("Error")
            is RepositoryResultState.Inited -> println("Inited")
            else -> println("Unknown")
        }


    }
}